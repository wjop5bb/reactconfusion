import React from 'react';
import {
    Card,
    Button,
    CardImg,
    CardImgOverlay,
    CardSubtitle,
    CardLink,
    CardText,
    CardBody,
    CardTitle,
    Breadcrumb, BreadcrumbItem ,
    Row, Col,
 Modal, ModalHeader, ModalBody, ModalFooter,
 Navbar, NavbarBrand, Nav, NavbarToggler, Collapse, NavItem, Jumbotron,

 Form, FormGroup, Input, Label } from "reactstrap";
  
  import Moment from "react-moment";
import { Link } from 'react-router-dom';
import { Control, LocalForm, Errors } from 'react-redux-form';
import { Loading } from './LoadingComponent';


function RenderDish ({dish, onClick}) {
    
    
    return (
        <Card>
        
                <CardImg width="100%" src={dish.image} alt={dish.name} />
                <CardImgOverlay>
                    <CardTitle>{dish.name}</CardTitle>
                </CardImgOverlay>
            
        </Card>
    );
  }
  const required = (val) => val && val.length;
const maxLength = (len) => (val) => !(val) || (val.length <= len);
const minLength = (len) => (val) => val && (val.length >= len);
const isNumber = (val) => !isNaN(Number(val));
const validEmail = (val) => /^[A-Z0-9._%+-]+@[A-Z0-9.-]+\.[A-Z]{2,4}$/i.test(val);
 class RenderComments extends React.Component{
    constructor(props) {
        super(props);
    this.toggleModal = this.toggleModal.bind(this);
        
        this.handleSubmit = this.handleSubmit.bind(this);
        this.state = {
           
            isModalOpen: false
        };
      }

    
      toggleModal() {
        this.setState({
          isModalOpen: !this.state.isModalOpen
        });
      }
      handleSubmit(values) {
        console.log('Current State is: ' + JSON.stringify(values));
        alert('Current State is: ' + JSON.stringify(values));
        this.props.addComment(this.props.dishId, values.rating, values.yourname, values.comment);
    
    }

     render(){  

  
        const renderUserList = () => {
            return  this.props.comments.map(comentariosrecorrer => {
              return <div>
              <br></br>
              <CardText>{comentariosrecorrer.comment} </CardText>
              <CardText>
                --{comentariosrecorrer.author},&nbsp;
                <Moment format="MMM/DD/YYYY">{comentariosrecorrer.date}</Moment>{" "}
              </CardText>
         
            </div>
            });
          };
       
    

        return (
        
            <div>
             { renderUserList() }
            <br></br>
            
            <Button outline color="secondary" onClick={this.toggleModal} ><i class="fa fa-pencil" aria-hidden="true"></i>
            <Modal isOpen={this.state.isModalOpen} toggle={this.toggleModal}>
                    <ModalHeader toggle={this.toggleModal}>Submit Comment</ModalHeader>
                    <ModalBody>
                    <LocalForm onSubmit={(values) => this.handleSubmit(values)}>
                
                     <Row className="form-group">
                     <Col md="12" md={{ size: 12, offset: 0      }}>
                                <Label htmlFor="Rating" >Rating</Label>
                              
                                    <Control.text model=".Rating" id="Rating" name="Rating"
                                        placeholder="Rating"
                                        className="form-control"
                                        type="number"
                                        validators={{
                                            required,  isNumber
                                        }}
                                         />
                                    <Errors
                                        className="text-danger"
                                        model=".firstname"
                                        show="touched"
                                        messages={{
                                            required: 'Required ',
                                   
                                        }}
                                     />
                                </Col>
                            </Row>
                            <Row className="form-group">
                            <Col md="12" md={{ size: 12, offset: 0      }}>
                                <Label htmlFor="yourname" >Your Name</Label>
                               
                                    <Control.text model=".yourname" id="yourname" name="yourname"
                                      placeholder="Your Name"
                                        className="form-control"
                                    
                                        validators={{
                                            required, minLength: minLength(3), maxLength: maxLength(15)
                                        }}
                                         />
                                    <Errors
                                        className="text-danger"
                                        model=".yourname"
                                        show="touched"
                                        messages={{
                                            required: 'Required ',
                                            minLength: 'Must be greater than 2 characters ',
                                            maxLength: 'Must be 15 characters or less '
                                        
                                        }}
                                     />
                                </Col>
                            </Row>
                            <Row className="form-group">
                            <Col md="12" md={{ size: 12, offset: 0      }}>
                                <Label htmlFor="comment" >Comment</Label>
                         
                                    <Control.textarea model=".comment" id="comment" name="comment"
                                       rows="5"
                                        className="form-control"
                                    
                                        validators={{
                                            required, minLength: minLength(50), maxLength: maxLength(160)
                                        }}
                                         />
                                    <Errors
                                        className="text-danger"
                                        model=".comment"
                                        show="touched"
                                        messages={{
                                            required: 'Required ',
                                            minLength: 'Must be greater than 49 Characters ',
                                            maxLength: 'Must be 160 Characters or less ',
                                       
                                        }}
                                     />
                                </Col>
                            </Row>
                            <Row className="form-group">
                                <Col md={{size:10, offset: 0}}>
                                    <Button type="submit" color="primary">
                                  Submit
                                    </Button>
                                </Col>
                            </Row>
                            </LocalForm>
                    </ModalBody>
                </Modal>
    &nbsp;
     Submit Comment</Button>
            
             </div>
                
        );
  

     }
 }
  /*function RenderComments ({comments, onClick}) {
 
    const comentarios = comments.map((comentariosrecorrer) => {
        return (
            <div>
                <br></br>
                <CardText>{comentariosrecorrer.comment} </CardText>
                <CardText>
                  --{comentariosrecorrer.author},&nbsp;
                  <Moment format="MMM/DD/YYYY">{comentariosrecorrer.date}</Moment>{" "}
                </CardText>
           
              </div>
        );
    });
    return (
        
        <div>
        {comentarios}
        <br></br>
        <Button outline color="secondary" onClick={ToggleModal}><i class="fa fa-pencil" aria-hidden="true"></i>
&nbsp;
 Submit Comment</Button>
        
         </div>
            
    );
  }
  */
function DishDetail(props) {
    if (props.isLoading) {
        return(
            <div className="container">
                <div className="row">            
                    <Loading />
                </div>
            </div>
        );
    }
    else if (props.errMess) {
        return(
            <div className="container">
                <div className="row">            
                    <h4>{props.errMess}</h4>
                </div>
            </div>
        );
    }
    else if (props.dish != null) 
            return (
                <div className="container">
                <div className="row">
                    <Breadcrumb>

                        <BreadcrumbItem><Link to="/menu">Menu</Link></BreadcrumbItem>
                        <BreadcrumbItem active>{props.dish.name}</BreadcrumbItem>
                    </Breadcrumb>
                    <div className="col-12">
                        <h3>{props.dish.name}</h3>
                        <hr />
                    </div>                
                </div>
                <div className="row">
                    <div className="col-12 col-md-5 m-1">
                        <RenderDish dish={props.dish} /> 
                    </div>
                    <div className="col-12 col-md-5 m-1">
                         <RenderComments comments={props.comments} addComment={props.addComment} dishId={props.dish.id}/>
                    </div>
                </div>
                </div>
            );
};

export default DishDetail;